import { Component } from "react"; 

class FormCOmponent extends Component{
inputFirstnameChangeHandler (event) {
 let value = event.target.value;
 console.log(value)
}
inputLastnameChangeHandler (event) {
 let value = event.target.value;
 console.log(value)
}
selectCountryChangeHandler (event) {
 let value = event.target.value;
 console.log(value)
}
inputSubjectChangeHandler (event) {
 let value = event.target.value;
 console.log(value)
}
onBtnSendDataClick(){
 console.log("Form đã được submit");
}
 render(){
  return (
   <div className="container1">
   <div className="row1">
     <div className="col-25">
       <label >First Name</label>
     </div>
     <div className="col-75">
       <input type="text" onChange={this.inputFirstnameChangeHandler}  name="firstname" placeholder="Your name.."/>
     </div>
   </div>
   <div className="row1">
     <div className="col-25">
       <label >Last Name</label>
     </div>
     <div className="col-75">
       <input type="text"onChange={this.inputLastnameChangeHandler} name="lastname" placeholder="Your last name.."/>
     </div>
   </div>
   <div className="row1">
     <div className="col-25">
       <label>Country</label>
     </div>
     <div className="col-75">
       <select onChange={this.selectCountryChangeHandler} name="country">
         <option value="australia">Australia</option>
         <option value="canada">Canada</option>
         <option value="usa">USA</option>
       </select>
     </div>
   </div>
   <div className="row1">
     <div className="col-25">
       <label>Subject</label>
     </div>
     <div className="col-75">
       <textarea onChange={this.inputSubjectChangeHandler} name="subject" id = "txtarea-subject" placeholder="Write something.."  style={{ height:"200px"}}></textarea>
     </div>
   </div>
   <div className="row1">
     <button onClick={this.onBtnSendDataClick} type="submit" className="button"> Send data </button>
   </div>
</div>
  )
 }
}

export default FormCOmponent;