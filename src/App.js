
import './App.css';
import FormComponent from './components/forms/FormComponent';
import TittleComponent from './components/tittles/TittleComponent';


function App() {
  return (
      <>
      <TittleComponent/>

        
      <FormComponent/>
      </>
  );
}

export default App;
